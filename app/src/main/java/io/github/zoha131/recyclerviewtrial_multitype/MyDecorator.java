package io.github.zoha131.recyclerviewtrial_multitype;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class MyDecorator extends RecyclerView.ItemDecoration {
    private int xOffset, yOffset;

    public MyDecorator(int offset) {
        this(offset, offset);
    }

    public MyDecorator(int xOffset, int yOffset) {
        this.xOffset = xOffset;
        this.yOffset = yOffset;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        outRect.set(xOffset,yOffset,xOffset,yOffset);
    }
}
