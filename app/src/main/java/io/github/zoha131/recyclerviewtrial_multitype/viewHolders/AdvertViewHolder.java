package io.github.zoha131.recyclerviewtrial_multitype.viewHolders;

import android.view.View;
import android.widget.TextView;

import io.github.zoha131.recyclerviewtrial_multitype.R;
import io.github.zoha131.recyclerviewtrial_multitype.data.Advert;

public class AdvertViewHolder extends BaseViewHolder<Advert> {

    TextView txtTite, txtDesc;
    public static final int TYPE = R.layout.advert_item;

    public AdvertViewHolder(View itemView) {
        super(itemView);

        txtTite = (TextView) itemView.findViewById(R.id.txtName);
        txtDesc = (TextView) itemView.findViewById(R.id.txtDesc);
    }

    @Override
    public void bind(Advert advert) {
        txtTite.setText(advert.getName());
        txtDesc.setText(advert.getDesc());
    }
}
