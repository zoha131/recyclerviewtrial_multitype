package io.github.zoha131.recyclerviewtrial_multitype.viewHolders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.Locale;

import io.github.zoha131.recyclerviewtrial_multitype.R;
import io.github.zoha131.recyclerviewtrial_multitype.data.Product;

public class ProductViewHolder extends BaseViewHolder<Product> {
    ImageView image;
    TextView title, desc, price;
    Product product;

    public static final int TYPE = R.layout.list_item;

    public ProductViewHolder(View itemView) {
        super(itemView);

        image = (ImageView) itemView.findViewById(R.id.imageView);
        title = (TextView) itemView.findViewById(R.id.titleTxt);
        desc = (TextView) itemView.findViewById(R.id.descText);
        price = (TextView) itemView.findViewById(R.id.price);
    }

    @Override
    public void bind(Product product) {

        this.product = product;

        RequestOptions requestOptions = new RequestOptions()
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background);
        Glide.with(image.getContext())
                .setDefaultRequestOptions(requestOptions)
                .load(product.getImage())
                .into(image);

        //holder.image.setImageResource(product.getImage());
        title.setText(product.getTitle());
        desc.setText(product.getDescription());
        price.setText(String.format(Locale.US, "%.2f", product.getPrice().floatValue()));
    }
}
