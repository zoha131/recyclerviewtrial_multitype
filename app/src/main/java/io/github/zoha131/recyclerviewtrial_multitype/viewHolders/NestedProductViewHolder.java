package io.github.zoha131.recyclerviewtrial_multitype.viewHolders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.Locale;

import io.github.zoha131.recyclerviewtrial_multitype.R;
import io.github.zoha131.recyclerviewtrial_multitype.data.Product;

public class NestedProductViewHolder extends BaseViewHolder<Product> {
    ImageView imageView;
    TextView priceTxt;

    public static final int TYPE = R.layout.nested_item;

    public NestedProductViewHolder(View itemView) {
        super(itemView);

        imageView = (ImageView) itemView.findViewById(R.id.imgNested);
        priceTxt = (TextView) itemView.findViewById(R.id.txtPrice);
    }

    @Override
    public void bind(Product product) {
        RequestOptions requestOptions = new RequestOptions()
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background);
        Glide.with(imageView.getContext())
                .setDefaultRequestOptions(requestOptions)
                .load(product.getImage())
                .into(imageView);

        priceTxt.setText(String.format(Locale.US, "%.2f$", product.getPrice().floatValue()));
    }
}
