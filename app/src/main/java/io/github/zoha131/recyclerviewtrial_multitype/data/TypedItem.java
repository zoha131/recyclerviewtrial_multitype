package io.github.zoha131.recyclerviewtrial_multitype.data;

public interface TypedItem {
    int getType(TypeFactory factory);
}
