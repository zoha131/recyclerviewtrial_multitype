package io.github.zoha131.recyclerviewtrial_multitype.data;

import android.media.UnsupportedSchemeException;
import android.view.View;

import io.github.zoha131.recyclerviewtrial_multitype.viewHolders.AdvertViewHolder;
import io.github.zoha131.recyclerviewtrial_multitype.viewHolders.BaseViewHolder;
import io.github.zoha131.recyclerviewtrial_multitype.viewHolders.NestedViewHolder;
import io.github.zoha131.recyclerviewtrial_multitype.viewHolders.ProductViewHolder;

public class TypeFactory {
    public int getType(Product product){
        return ProductViewHolder.TYPE;
    }

    public int getType(Advert advert){
        return AdvertViewHolder.TYPE;
    }

    public int getType(NestedModel nestedModel){
        return NestedViewHolder.TYPE;
    }

    public BaseViewHolder createViewHolder(int type, View view) {

        BaseViewHolder viewHolder;

        switch (type){
            case ProductViewHolder.TYPE:
                viewHolder = new ProductViewHolder(view);
                break;
            case AdvertViewHolder.TYPE:
                viewHolder = new AdvertViewHolder(view);
                break;
            case NestedViewHolder.TYPE:
                viewHolder = new NestedViewHolder(view);
                break;
            default:
                throw new RuntimeException("The type is not supported yet");

        }

        return viewHolder;
    }
}
