package io.github.zoha131.recyclerviewtrial_multitype.data;

public class Advert implements TypedItem {
    String name, desc;

    public Advert(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    @Override
    public int getType(TypeFactory factory) {
        return factory.getType(this);
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }
}
