package io.github.zoha131.recyclerviewtrial_multitype.data;

public class NestedModel implements TypedItem {
    Product[] data;

    public NestedModel() {
        Products products = new Products();
        this.data = products.PRODUCTS;
    }

    public Product[] getData() {
        return data;
    }

    @Override
    public int getType(TypeFactory factory) {
        return factory.getType(this);
    }
}
