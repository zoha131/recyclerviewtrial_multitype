package io.github.zoha131.recyclerviewtrial_multitype.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import io.github.zoha131.recyclerviewtrial_multitype.data.TypedItem;

public abstract class BaseViewHolder<T extends TypedItem> extends RecyclerView.ViewHolder {
    public BaseViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void bind(T t);
}
