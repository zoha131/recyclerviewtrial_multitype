package io.github.zoha131.recyclerviewtrial_multitype;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import io.github.zoha131.recyclerviewtrial_multitype.data.TypeFactory;
import io.github.zoha131.recyclerviewtrial_multitype.data.TypedItem;
import io.github.zoha131.recyclerviewtrial_multitype.viewHolders.BaseViewHolder;

public class MutiTypedAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    ArrayList<TypedItem> data;
    TypeFactory typeFactory;

    public MutiTypedAdapter(ArrayList<TypedItem> data) {
        this.data = data;
        typeFactory = new TypeFactory();
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);

        return typeFactory.createViewHolder(viewType, view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        //unchecked
        holder.bind(data.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).getType(typeFactory);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
