package io.github.zoha131.recyclerviewtrial_multitype.data;

import java.util.ArrayList;

public class TypedItems {


    public static ArrayList<TypedItem> getData(){
        ArrayList<TypedItem> data = new ArrayList<>();
        data.add(Products.BLACK_HAT);
        data.add(new Advert("Nokia", "Nokia was a best phone company"));
        data.add(Products.BLACK_SHIRT_MALE);
        data.add(new Advert("SamSung", "Samsung one of the best brand"));
        data.add(Products.BLACK_SHIRT_FEMALE);
        data.add(new NestedModel());
        data.add(new Advert("Iphone", "Apple is the richest company in the world"));
        data.add(Products.YELLOW_LAMP);
        data.add(new Advert("Pixel", "Google landed in phone company by pixel"));
        data.add(Products.WHITE_SHIRT_MALE);
        data.add(Products.WHITE_SHIRT_FEMALE);
        data.add(Products.WHITE_MUG);
        data.add(Products.WHITE_HAT);
        data.add(Products.RED_MUG);
        data.add(Products.RED_LAMP);
        data.add(Products.ORANGE_HAT);
        data.add(Products.ICEY_COAST_PICTURE);
        data.add(Products.ICELAND_PICTURE);


        return data;
    }
}
