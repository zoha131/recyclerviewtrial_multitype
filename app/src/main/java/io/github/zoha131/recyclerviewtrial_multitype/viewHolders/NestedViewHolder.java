package io.github.zoha131.recyclerviewtrial_multitype.viewHolders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import io.github.zoha131.recyclerviewtrial_multitype.MyDecorator;
import io.github.zoha131.recyclerviewtrial_multitype.R;
import io.github.zoha131.recyclerviewtrial_multitype.data.NestedModel;
import io.github.zoha131.recyclerviewtrial_multitype.data.Product;
import io.github.zoha131.recyclerviewtrial_multitype.data.Products;

public class NestedViewHolder extends BaseViewHolder<NestedModel> {

    private static final String TAG = "NestedViewHolder";

    RecyclerView nestedRecyclerView;
    Context context;
    LinearSnapHelper snapHelper;
    LinearLayoutManager layoutManager;
    NestedAdapter adapter;
    MyDecorator myDecorator;
    RecyclerView.RecycledViewPool recycledViewPool;

    public static final int TYPE = R.layout.nested_main;

    public NestedViewHolder(View itemView) {
        super(itemView);
        Log.d(TAG, "OneCreateViewHolder");
        nestedRecyclerView = (RecyclerView) itemView.findViewById(R.id.nested_recyclerView);
        context = itemView.getContext();

        layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        layoutManager.setItemPrefetchEnabled(true);
        layoutManager.setInitialPrefetchItemCount(9);
        recycledViewPool = new RecyclerView.RecycledViewPool();//This is very important for performance.
        adapter = new NestedAdapter();
        myDecorator = new MyDecorator(10, 0);
        snapHelper = new LinearSnapHelper();

        nestedRecyclerView.setHasFixedSize(true);
        nestedRecyclerView.setNestedScrollingEnabled(false);
        nestedRecyclerView.addItemDecoration(myDecorator);
        nestedRecyclerView.setLayoutManager(layoutManager);
        nestedRecyclerView.setRecycledViewPool(recycledViewPool);//this line must be before setAdapterMethod. or use this way.
        //TODO Nested recyclerView in first time or loading time has some performance issue. Scroll is not smooth for the first time.
    }

    @Override
    public void bind(NestedModel nestedModel) {

        Log.d(TAG, "OnBindVIewHolder");

        adapter.setData(nestedModel.getData());
        nestedRecyclerView.setAdapter(adapter);
        snapHelper.attachToRecyclerView(nestedRecyclerView);

    }
}

class NestedAdapter extends RecyclerView.Adapter<NestedProductViewHolder>{

    Product[] data;
    private static final String TAG = "NestedAdapter";

    public NestedAdapter() {

    }

    public void setData(Product[] data) {
        this.data = data;
    }

    @NonNull
    @Override
    public NestedProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "OneCreateViewHolder");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nested_item, parent, false);
        return new NestedProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NestedProductViewHolder holder, int position) {
        Log.d(TAG, "OnBindVIewHolder");
        holder.bind(data[position]);
    }

    @Override
    public int getItemCount() {
        return data.length;
    }
}
